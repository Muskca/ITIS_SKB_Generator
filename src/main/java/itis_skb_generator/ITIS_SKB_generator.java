/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itis_skb_generator;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentProducer;
import org.apache.http.entity.EntityTemplate;
import org.apache.jena.atlas.web.ContentType;
import org.apache.jena.graph.Graph;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Fabien
 */
public class ITIS_SKB_generator {
    
    
    public static HashMap<String, String> classMappings;
    public static String baseUri = "http://ontology.irstea.fr/agronomictaxon/SKB/ITIS#";
    public static String taxonomyUri = baseUri+"ITIS_Thesaurus";
    
    
    public static String cleanString(String s){
        return s.replaceAll(" ", "_").replaceAll("\\.", "");
    }
    
    public static String generateTriple(String subject, String predicate, String object){
        String ret = "<"+subject+">";
        if(predicate.equals("a")){
            String type = classMappings.get(object);
            boolean newClass= false;
            if(type == null){
                type = baseUri+cleanString(object);
                newClass = true;
            }
            ret += " <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"+type+">.\n";
            if(newClass){
                ret += "<"+type+"> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://ontology.irstea.fr/agronomictaxon/core#Taxon>.\n";
            }
        }
        else if(predicate.equals("rdfs:label")){
            ret += " <http://www.w3.org/2000/01/rdf-schema#label> "+object+".\n";
        }
        else{
            ret += " "+predicate+" ";
            if(ret.startsWith("http"))
                ret += "<"+object+">.\n";
            else
                ret += object+".\n";
        }
        
        
        return ret;
    }
    
     public static HttpEntity graphToHttpEntity(final Graph graph) {
        final RDFFormat syntax = RDFFormat.TURTLE_BLOCKS ;
        ContentProducer producer = new ContentProducer() {
            @Override
            public void writeTo(OutputStream out) {
                RDFDataMgr.write(out, graph, syntax) ;
            }
        } ;
        EntityTemplate entity = new EntityTemplate(producer) ;
        ContentType ct = syntax.getLang().getContentType() ;
        entity.setContentType(ct.getContentType()) ;
        return entity ;
    }
    
      public static StringBuilder concatTaxon(String url){
        StringBuilder ret = new StringBuilder();
         try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            String termString = doc.select(".taxon_head i").text();
            
            Elements links = doc.select("table");
            Element e = links.get(1);
            Element taxonTable = e.select("table").first();
            String rank = taxonTable.select("tr").get(3).select("td").get(2).text();
            //System.out.println("TERM : "+termString);
            String taxonUri = baseUri+cleanString(termString);

            ret.append(generateTriple(taxonUri, "a", rank));
            ret.append("<"+taxonomyUri+"> <http://ontology.irstea.fr/AgronomicTaxon#memberScheme> <"+taxonUri+">.\n");
            ret.append("<"+taxonUri+"> <http://www.w3.org/2004/02/skos/core#inScheme> <"+taxonomyUri+">.\n");
            ret.append("<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <"+url+">.\n");
            System.out.println(termString+" -- "+rank);
            
            ArrayList<String> synonymes = new ArrayList<>();
            boolean again = true;
            Elements trs = taxonTable.select("tr");
            int nbTr = trs.size();
            int i = 4;
            String currentType = null;
            while(again && i < nbTr){
                Element tr = trs.get(i);
                Elements tds = tr.select("td");
                String typeElem = tds.get(1).text().trim();
                if(typeElem.contains("Synonym") || typeElem.contains("Common Name") || typeElem.equals(" ")){
                    Elements as = tds.get(2).select("a");
                    String syn = null;
                    if(!as.isEmpty())
                        syn = as.first().text();
                    else
                        syn = tds.get(2).text();
                    if(syn != null && !syn.equals(" ") &&  !syn.trim().isEmpty()){
                        synonymes.add(syn);
                        ret.append(generateTriple(taxonUri, "rdfs:label", "\""+syn+"\""));
                        if(typeElem.contains("Synonym")){
                            currentType = "syn";
                        }
                        else if(typeElem.contains("Common Name")){
                            currentType = "common";
                        }
                        if(currentType != null && currentType.equals("syn")){
                            ret.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasScientificName>", "\""+syn+"\""));
                        }
                        else if(currentType != null && currentType.equals("common")){
                            ret.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasVernacularName>", "\""+syn+"\""));
                        }
                    }
                }
                else{
                    again = false;
                } 
                i++;
            }
            
            ArrayList<String> narrowers = new ArrayList<>();
            Elements trsNar = doc.select("table").get(3).select("table").first().select("tr");
            boolean isChildren = false;
            i = 0;
            while(i < trsNar.size()){
                Element td = trsNar.get(i).select("td").get(2);
                if(!isChildren){
                    isChildren = td.text().contains("Direct Children");
                }
                else{
                    Element a = td.select("a").first();
                    if(a != null){
                        narrowers.add(a.attr("href"));
                        ret.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank>", "<"+baseUri+cleanString(a.text())+">"));
                        ret.append(generateTriple(baseUri+cleanString(a.text()), "<http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank>", "<"+taxonUri+">"));
                    }
                }
                i++;
            }
            for(String urlNar : narrowers){
                ret.append(concatTaxon("https://www.itis.gov/servlet/SingleRpt/"+urlNar));
            }
            
        } catch (IOException ex) {
            System.err.println("ERROR GETTING URL CONTENT : "+url);
        }
         return ret;
    }
     
      public static String getTaxonTriples(String url, String upperTaxonUri, StringBuilder retGlobal){
          StringBuilder ret = new StringBuilder();
          String taxonUri = null;
         try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            String termString = doc.select(".taxon_head").text().trim();
            
            Elements links = doc.select("table");
            Element e = links.get(1);
            Element taxonTable = e.select("table").first();
            String rank = taxonTable.select("tr").get(3).select("td").get(2).text();
            //System.out.println("TERM : "+termString);
            taxonUri = baseUri+cleanString(termString);

            ret.append(generateTriple(taxonUri, "a", rank));
            ret.append("<"+taxonomyUri+"> <http://ontology.irstea.fr/AgronomicTaxon#memberScheme> <"+taxonUri+">.\n");
            ret.append("<"+taxonUri+"> <http://www.w3.org/2004/02/skos/core#inScheme> <"+taxonomyUri+">.\n");
            ret.append("<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <"+url+">.\n");
            System.out.println(termString+" -- "+rank);
            
            ArrayList<String> synonymes = new ArrayList<>();
            boolean again = true;
            Elements trs = taxonTable.select("tr");
            int nbTr = trs.size();
            int i = 4;
            String currentType = null;
            while(again && i < nbTr){
                Element tr = trs.get(i);
                Elements tds = tr.select("td");
                String typeElem = tds.get(1).text().trim();
                if(typeElem.contains("Synonym") || typeElem.contains("Common Name") || typeElem.equals(" ")){
                    Elements as = tds.get(2).select("a");
                    String syn = null;
                    if(!as.isEmpty())
                        syn = as.first().text();
                    else
                        syn = tds.get(2).text();
                    if(syn != null && !syn.equals(" ") &&  !syn.trim().isEmpty()){
                        synonymes.add(syn);
                        ret.append(generateTriple(taxonUri, "rdfs:label", "\""+syn+"\""));
                        if(typeElem.contains("Synonym")){
                            currentType = "syn";
                        }
                        else if(typeElem.contains("Common Name")){
                            currentType = "common";
                        }
                        if(currentType != null && currentType.equals("syn")){
                            ret.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasScientificName>", "\""+syn+"\""));
                        }
                        else if(currentType != null && currentType.equals("common")){
                            ret.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasVernacularName>", "\""+syn+"\""));
                        }
                    }
                }
                else{
                    again = false;
                } 
                i++;
            }
            if(upperTaxonUri != null){
                ret.append(generateTriple("<"+taxonUri+">", "<http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank>", "<"+upperTaxonUri+">"));
                ret.append(generateTriple("<"+upperTaxonUri+">", "<http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank>", "<"+taxonUri+">"));
            }
            
//            ArrayList<String> narrowers = new ArrayList<>();
//            Elements trsNar = doc.select("table").get(3).select("table").first().select("tr");
//            boolean isChildren = false;
//            i = 0;
//            while(i < trsNar.size()){
//                Element td = trsNar.get(i).select("td").get(2);
//                if(!isChildren){
//                    isChildren = td.text().contains("Direct Children");
//                }
//                else{
//                    Element a = td.select("a").first();
//                    if(a != null){
//                        narrowers.add(a.attr("href"));
//                        ret.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank>", "<"+baseUri+cleanString(a.text())+">"));
//                        ret.append(generateTriple(baseUri+cleanString(a.text()), "<http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank>", "<"+taxonUri+">"));
//                    }
//                }
//                i++;
//            }
        } catch (IOException ex) {
            System.err.println("ERROR GETTING URL CONTENT : "+url);
        }
         retGlobal.append(ret);
         return taxonUri;
      }
      
      
    public static StringBuilder concatUpperTaxon(String url){
        StringBuilder ret = new StringBuilder();
         try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            
            ArrayList<String> narrowers = new ArrayList<>();
            Elements trsNar = doc.select("table").get(3).select("table").first().select("tr");
            boolean isUpper = true;
            int i = 0;
            while(i < trsNar.size() && isUpper){
                Element td = trsNar.get(i).select("td").get(2);
                isUpper = !td.text().contains("Direct Children");
                if(isUpper){
                    Element a = td.select("a").first();
                    String taxonUri = null;
                    if(a != null){
                        String urlUpper = a.attr("href");
                        taxonUri = getTaxonTriples("https://www.itis.gov/servlet/SingleRpt/"+urlUpper, taxonUri, ret);
                    }
                }
                i++;
            }
        } catch (IOException ex) {
            System.err.println("ERROR GETTING URL CONTENT : "+url);
        }
         return ret;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       classMappings = new HashMap<>();
       classMappings.put("Genus", "http://ontology.irstea.fr/agronomictaxon/core#GenusRank");
       classMappings.put("Species", "http://ontology.irstea.fr/agronomictaxon/core#SpecyRank");
       classMappings.put("Family", "http://ontology.irstea.fr/agronomictaxon/core#FamilyRank");
       classMappings.put("Order", "http://ontology.irstea.fr/agronomictaxon/core#OrderRank");
       classMappings.put("Class", "http://ontology.irstea.fr/agronomictaxon/core#ClassRank");
       classMappings.put("Division", "http://ontology.irstea.fr/agronomictaxon/core#DivisionRank");
       classMappings.put("Kingdom", "http://ontology.irstea.fr/agronomictaxon/core#KingdomRank");
       //classMappings.put("Subspecies Subspecies", baseUri+"SUBSPECIES/");
        
         StringBuilder out = new StringBuilder();
        System.out.println("Export ontological module prefixes ... ");
        try {
            String modulePrefixes = FileUtils.readFileToString(new File("in/agronomicTaxon_prefix.ttl"), "utf-8");
            out.append(modulePrefixes);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Prefixes exported");
        
        System.out.println("Export ontological module ... ");
        try {
            String moduleTriples = FileUtils.readFileToString(new File("in/agronomicTaxon_2.ttl"), "utf-8");
            out.append(moduleTriples);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Module exported");
        
        
       out.append("<"+baseUri+"> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology>  .\n");
       
       out.append("<"+taxonomyUri+"> a <http://ontology.irstea.fr/agronomictaxon/core#Taxonomy>.\n");
       out.append(concatUpperTaxon("https://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=42236"));
       StringBuilder concat = concatTaxon("https://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=42236");
       out.append(concat);
       System.out.println(out);
        try {
            FileUtils.write(new File("out_ITIS.ttl"), out);
        } catch (IOException ex) {
            Logger.getLogger(ITIS_SKB_generator.class.getName()).log(Level.SEVERE, null, ex);
        }
//       DatasetGraph dsg = DatasetGraphFactory.create();
//        DataService dataService = new DataService(dsg) ;
//        dataService.addEndpoint(OperationName.Query, "");
//        dataService.addEndpoint(OperationName.Update, "");
//
//        FusekiEmbeddedServer server = FusekiEmbeddedServer.create()
//           .setPort(3332)
//           .add("/CABI", dataService)
//           .build() ;
//        server.start() ;
//
//        Graph g = SSE.parseGraph("(graph "+ret+")") ;
//        HttpEntity e = graphToHttpEntity(g) ;
//        HttpOp.execHttpPut("http://localhost:3332/CABI", e) ;
//       DatasetGraph dsg = DatasetGraphFactory.create();
//        DataService dataService = new DataService(dsg) ;
//        dataService.addEndpoint(OperationName.Query, "");
//        dataService.addEndpoint(OperationName.Update, "");
//
//        FusekiEmbeddedServer server = FusekiEmbeddedServer.create()
//           .setPort(3332)
//           .add("/CABI", dataService)
//           .build() ;
//        server.start() ;
//
//        Graph g = SSE.parseGraph("(graph "+ret+")") ;
//        HttpEntity e = graphToHttpEntity(g) ;
//        HttpOp.execHttpPut("http://localhost:3332/CABI", e) ;
        
    }
}
